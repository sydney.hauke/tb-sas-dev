#include "sas_dev.h"

// FIXME : global variables are nasty. 
// We simulate our global memory
void *global_mem = NULL;
// Handle for dynamically loaded library
void *dl_handle = NULL;
// POCL workgroup function
pocl_workgroup wg = NULL;

// Pipe file descriptors
int data_wr_fifo, data_rd_fifo, cmd_fifo, rsp_fifo;

// Socket file descriptors
int data_socket_fd; // socket fd
int cmd_socket_fd; // socket fd
int data_socket, cmd_socket; // accepted connections fd

void 
sigint_handler(int signo)
{
    close(data_wr_fifo);
    close(data_rd_fifo);
    close(cmd_fifo);
    close(rsp_fifo);

    remove(DATA_RD_PIPE_PATHNAME);
    remove(DATA_WR_PIPE_PATHNAME);
    remove(CMD_PIPE_PATHNAME);
    remove(RSP_PIPE_PATHNAME);

    _exit(signo);
}

void
help(char *progname)
{
    printf("Usage: %s s|p\n         \
            \ts : use sockets\n    \
            \tp : use pipes\n", progname);
}

int
set_wg_func(char *dl_obj_path, char *sym_name)
{
    int ret = 0;

    dl_handle = dlopen(dl_obj_path, RTLD_NOW);
    if(dl_handle == NULL) {
        fprintf(stderr, "%s\n", dlerror());
        ret = -1;
        goto _dlopen_err;
    }

    wg = dlsym(dl_handle, sym_name);
    if(wg == NULL) {
        fprintf(stderr, "%s\n", dlerror());
        ret = -1;
        goto _dlsym_err;
    }

    goto _out;

_dlsym_err:
    dlclose(dl_handle);
_dlopen_err:
_out:
    return ret;
}

int
wait_connection()
{
    struct sockaddr address;
    int addrlen = sizeof(address);

    /* Wait for connection on data socket */
    printf("Waiting for connection on data port\n");
    data_socket = accept(data_socket_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen);
    PERROR_AND_RETURN_ON((data_socket < 1), -1, "data port accept");

    /* Wait for connection on cmd socket */
    printf("Waiting for connections on cmd port\n");
    cmd_socket = accept(cmd_socket_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen);
    PERROR_AND_RETURN_ON((cmd_socket < 1), -1, "command port accept");

    data_rd_fifo = data_socket;
    data_wr_fifo = data_socket;

    cmd_fifo = cmd_socket;
    rsp_fifo = cmd_socket;

    return 0;
}

int
check_connection()
{
    // TODO
    return 0; // connection alive
}

int 
init(int argc, char *argv[])
{
    int ret;

    struct sockaddr_in address;

    /* Setup pipes or sockets */

    if(argc < 2) {
        puts("Not enough arguments\n");
        help(argv[0]);
        goto _out;
    }

    if(strcmp(argv[1], "s") == 0) {
        /* Use sockets */

        printf("Using sockets to communicate\n");

        data_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
        cmd_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
        if((data_socket_fd < 0) | (cmd_socket_fd < 0)) {
            perror("socket");
            goto _out;
        }        

        int opt = 1;
        ret = setsockopt(data_socket_fd, 
                         SOL_SOCKET, 
                         SO_REUSEADDR|SO_REUSEPORT|SO_KEEPALIVE, 
                         &opt, 
                         sizeof(opt)); 
        if(ret < 0) {
            perror("setsockopt");
            goto _out;
        }
        ret = setsockopt(cmd_socket_fd, 
                         SOL_SOCKET, 
                         SO_REUSEADDR|SO_REUSEPORT|SO_KEEPALIVE, 
                         &opt, 
                         sizeof(opt)); 
        if(ret < 0) {
            perror("setsockopt");
            goto _out;
        }

        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons(DATA_PORT);
        ret = bind(data_socket_fd, (struct sockaddr*)&address, sizeof(address));        
        if(ret < 0) {
            perror("bind");
            goto _out;
        }

        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons(CMD_PORT);
        ret = bind(cmd_socket_fd, (struct sockaddr*)&address, sizeof(address));        
        if(ret < 0) {
            perror("bind");
            goto _out;
        }

        ret = listen(data_socket_fd, 16);
        if(ret < 0) {
            perror("listen");
            goto _out;
        }

        ret = listen(cmd_socket_fd, 16);
        if(ret < 0) {
            perror("listen");
            goto _out;
        }

    }
    else if(strcmp(argv[1], "p") == 0) {
        /* Use pipes */
        
        printf("Using pipes to communicate\n");

        ret = mkfifo(DATA_RD_PIPE_PATHNAME, 0666);
        if (ret) {
            perror("data read pipe");
            goto _out;
        }

        ret = mkfifo(DATA_WR_PIPE_PATHNAME, 0666);
        if (ret) {
            perror("data write pipe");
            goto _out;
        }

        ret = mkfifo(CMD_PIPE_PATHNAME, 0666);
        if (ret) {
            perror("cmd pipe");
            goto _out;
        }

        ret = mkfifo(RSP_PIPE_PATHNAME, 0666);
        if (ret) {
            perror("rsp pipe");
            goto _out;
        }

        printf("Waiting for a command writer...\n");

        cmd_fifo = open(CMD_PIPE_PATHNAME, O_RDONLY);
        if (cmd_fifo < 0) {
            perror("error while opening cmd pipe");
            ret = EXIT_FAILURE;
            goto _out;
        }

        printf("Waiting for a response reader...\n");

        rsp_fifo = open(RSP_PIPE_PATHNAME, O_WRONLY);
        if (rsp_fifo < 0) {
            perror("error while opening response pipe");
            ret = EXIT_FAILURE;
            goto _out;
        }

        printf("Waiting for a data writer...\n");

        data_wr_fifo = open(DATA_WR_PIPE_PATHNAME, O_RDONLY);
        if (data_wr_fifo < 0) {
            perror("error while opening data write pipe");
            ret = EXIT_FAILURE;
            goto _out;
        }

        printf("Waiting for a data reader...\n");

        /* Blocks until a reader opens this pipe */
        data_rd_fifo = open(DATA_RD_PIPE_PATHNAME, O_WRONLY);
        if (data_rd_fifo < 0) {
            perror("error while opening data read pipe");
            ret = EXIT_FAILURE;
            goto _out;
        }

        printf("Successfully setup pipes\n");
    }
    else {
        /* Wrong arguments */
        puts("Incorrect arguments\n");
        help(argv[0]);
        return -1;
    }

    /* Setup signal handler, so that pipes are correctly closed and 
     * deleted from the FS before the program exits */
    if(signal(SIGINT, sigint_handler)) {
        perror("SIGINT handler");
        goto _out;
    }

    /* Allocate "global memory" visible by the OpenCL implementation */
    global_mem = malloc(GLOBAL_MEM_SIZE);
    if(global_mem == NULL) {
        perror("global memory");
        goto _out;
    }

    #ifdef DEBUG
        printf("global_mem address: %p\n", global_mem);
    #endif

    return 0;

_out:
    return -1;
}

int
cmd_read(uint64_t src_addr, uint64_t len)
{
    /* On 32-bit ARM systems, the 64-bit address is cast to a 32 bit address */
    void *src_mem = (void*)src_addr;

    ssize_t ret = write_pipe(data_rd_fifo, src_mem, len);
    PERROR_AND_RETURN_ON((ret < 0), -1, "Writing data to read pipe");

    return 0;
}


int
cmd_write(uint64_t dest_addr, uint64_t len)
{
    /* On 32-bit ARM systems, the 64-bit address is cast to a 32 bit address */
    void *dest_mem = (void*)dest_addr;

    ssize_t ret = read_pipe(data_wr_fifo, dest_mem, len);
    PERROR_AND_RETURN_ON((ret < 0), -1, "Reading data from write pipe");

    return 0;
}

int
cmd_set_args(void ***arguments,
             uint64_t num_args)
{
    ssize_t ret;
    uint64_t *args64;
    size_t args64_size = num_args * (sizeof(uint64_t));

    args64 = malloc(args64_size);
    PERROR_AND_RETURN_ON(args64 == NULL, -1, "arguments malloc");

    /* (Re)allocation of arguments */
    if(*arguments == NULL) {
        *arguments = malloc(num_args * sizeof(void*));
        PERROR_AND_RETURN_ON((*arguments == NULL), -1, "Allocation for arguments");
    }
    else {
        // TODO : free all allocated arguments in each array element
        *arguments = realloc(*arguments, num_args * sizeof(void*));
        PERROR_AND_RETURN_ON((*arguments == NULL), -1, "Reallocation for arguments");
    }

    ret = read_pipe(data_wr_fifo, args64, args64_size);
    PERROR_AND_RETURN_ON((ret < 0), -1, "Reading arguments through write pipe");

    // Set an indirect reference to each argument because the workgroup function expects
    // that.
    for(size_t i = 0; i < num_args; i++) {
        void *arg = (void*)(args64[i]); // copy and cast 64 bit argument
        (*arguments)[i] = malloc(sizeof(void*));
        PERROR_AND_RETURN_ON(((*arguments)[i] == NULL), -1, "Indirect allocation");
        *((void**)(*arguments)[i]) = arg;
    }

    return 0;
}

int
cmd_set_dl_obj(uint64_t so_len, uint64_t symname_len)
{
    int ret = 0;
    char *dl_path;
    char *sym_name;
    ssize_t read_ret;

    if (so_len >= FILE_PATHNAME_LENGTH) {
        fprintf(stderr, "File pathname is too big\n");
        ret = -1;
        goto _ret;
    }

    if(symname_len >= SYM_NAME_LENGTH) {
        fprintf(stderr, "symbol name is too big\n");
        ret = -1;
        goto _ret;
    }

    dl_path = malloc(FILE_PATHNAME_LENGTH);
    if(dl_path == NULL) {
        perror("dl_path");
        ret = -1;
        goto _ret;
    }

    sym_name = malloc(SYM_NAME_LENGTH);
    if(sym_name == NULL) {
        perror("sym_name");
        ret = -1;
        goto _malloc_err;
    }

    read_ret = read_pipe(data_wr_fifo, dl_path, so_len);
    if(read_ret < 0) {
        perror("Error while reading dl pathname");
        ret = -1;
        goto _read_pipe_err;
    }

    read_ret = read_pipe(data_wr_fifo, sym_name, symname_len);
    if(read_ret < 0) {
        perror("Error while reading symbol name");
        ret = -1;
        goto _read_pipe_err;
    }

    ret = set_wg_func(dl_path, sym_name);

_read_pipe_err:
    free(sym_name);
_malloc_err:
    free(dl_path);
_ret:
    return ret;
}

int
cmd_set_context(struct pocl_context *pc, uint64_t pc_size)
{
    /* PC struct length is in number of bytes */
    ssize_t ret;
    struct pocl_sas_context psc;
    ret = read_pipe(data_wr_fifo, &psc, pc_size);
    PERROR_AND_RETURN_ON((ret < 0), -1, "Reading POCL context from data write pipe");

    sas_to_pocl_context(pc, &psc);

    return 0;
}

int
cmd_start_comp(void **arguments, struct pocl_context *pc)
{
    wg(arguments, pc);

    return 0;
}

int
cmd_get_global_base_addr()
{
    ssize_t ret;
    uint64_t addr = (uint64_t)global_mem; // the SAS driver wants to know a 64-bit address
    ret = write_pipe(data_rd_fifo, &addr, sizeof(addr));
    PERROR_AND_RETURN_ON((ret < 0), -1, "Writing global memory address to data read pipe");

    return 0;
}

int
cmd_send_and_set_dl_obj(int *fd, uint64_t dl_obj_len, uint64_t sym_name_len)
{
    ssize_t read_ret;
    int ret = 0;
    void *mmaped_file;
    char *sym_name;

    if(sym_name_len > SYM_NAME_LENGTH) {
        fputs("Symbol name too big\n", stderr);
        ret = -1;
        goto _out;
    }

    sym_name = malloc(SYM_NAME_LENGTH);
    if(sym_name == NULL) {
        perror("symbol name alloc");
        ret = -1;
        goto _out;
    }

    /* Create temporary file. If already existing, overwrite it */
    *fd = open(SERIALIEZED_KERNEL_PATH, 
               O_CREAT|O_RDWR|O_APPEND|O_TRUNC,
               S_IRWXU);
    if(*fd < 0) {
        perror("kernel open");
        ret = -1;
        goto _open_err;
    }

    /* Give enough space in file */
    ret = ftruncate(*fd, dl_obj_len);
    if(ret < 0) {
        perror("truncate kernel file");
        ret = -1;
        goto _truncate_err;
    }

    mmaped_file = mmap(NULL, 
                       dl_obj_len + (dl_obj_len % sysconf(_SC_PAGE_SIZE)), 
                       PROT_WRITE|PROT_READ, 
                       MAP_SHARED, 
                       *fd, 
                       0);
    if(mmaped_file == MAP_FAILED) {
        perror("mmap kernel file");
        ret = -1;
        goto _mmap_err;
    }

    /* Read dl object */
    read_ret = read_pipe(data_wr_fifo, mmaped_file, dl_obj_len);
    if(read_ret < 0) {
        perror("receiving dl obj");
        ret = -1;
        goto _read_dl_obj_err;
    }

    /* Flush entire dl obj on disk */
    fsync(*fd);

    /* Read symbol name of work-group launcher */
    read_ret = read_pipe(data_wr_fifo, sym_name, sym_name_len);
    if(read_ret < 0) {
        perror("receiving symbol name");
        ret = -1;
        goto _read_sym_err; 
    }

    ret = set_wg_func(SERIALIEZED_KERNEL_PATH, sym_name);

_read_sym_err:
_read_dl_obj_err:
    munmap(mmaped_file, dl_obj_len);
_truncate_err:
_mmap_err:
_open_err:
    free(sym_name);
_out:
    return ret;
}

int
rsp_write(uint8_t code)
{
    ssize_t ret = write_pipe(rsp_fifo, &code, RSP_PACKET_SIZE);
    PERROR_AND_RETURN_ON((ret < 0), -1, "Sending a response code through response pipe");

    return 0;
}


int 
main(int argc, char *argv[])
{
    int ret = 0;
    uint8_t cmd;
    uint64_t arg0, arg1;
    uint8_t cmd_packet[CMD_PACKET_SIZE];
    void **arguments = NULL;
    struct pocl_context pc;
    int is_socket = 0;
    int kernel_fd;

    if(init(argc, argv)) {
        ret = EXIT_FAILURE;
        goto _out;
    }

    is_socket = !strcmp(argv[1], "s");
    if(is_socket) {
        wait_connection();
    }

    while(1) {
        ssize_t read_ret;

        // TODO/FIXME : determining if connection is still alive
        if(is_socket && check_connection()) {
            wait_connection();
        }

        #ifdef DEBUG
        fputs("Waiting for next command...\n", stdout);
        #endif

        /* wait and read next command */
        read_ret = read_pipe(cmd_fifo, &cmd_packet, CMD_PACKET_SIZE);
        /* FIXME : keep the program running if there is no fatal crash */
        if(read_ret < 0) {
            ret = EXIT_FAILURE;
            goto _out;
        }

        /* execute command */
        cmd = *((uint8_t*)cmd_packet);
        arg0 = *(uint64_t*)(cmd_packet + 1); 
        arg1 = *(uint64_t*)(cmd_packet + 1 + sizeof(uint64_t)); 

        switch(cmd) {
            case CMD_RD_BUF:
                /* arg0 : src address in global memory
                 * arg1 : read length
                 */
                // TODO : check read length and memory boundaries
                ret = cmd_read(arg0, arg1); 
                break;

            case CMD_WR_BUF:
                /* arg0 : dest address in global memory
                 * arg1 : write length
                 */
                // TODO : check write length and memory boundaries
                ret = cmd_write(arg0, arg1);
                break;

            case CMD_SET_DL_OBJ_PATH:
                /* arg0 : pathname length
                 * arg1 : workgroup function symbolname length
                */
                ret = cmd_set_dl_obj(arg0, arg1); 
                break;

            case CMD_SET_ARGS:
                /* arg0 : number of arguments
                 */
                ret = cmd_set_args(&arguments, arg0);
                break;
            
            case CMD_SET_CONTEXT:
                /* arg0 : size of object
                 */
                ret = cmd_set_context(&pc, arg0);
                break;

            case CMD_START_COMP:
                ret = cmd_start_comp(arguments, &pc);
                break;

            case CMD_GET_GLOBAL_BASE_ADDR:
                ret = cmd_get_global_base_addr();
                break;

            case CMD_SEND_AND_SET_DL_OBJ:
                /* arg0 : dl. obj. length
                 * arg1 : sym. name length
                 */
                ret = cmd_send_and_set_dl_obj(&kernel_fd, arg0, arg1);
                break;

            default:
                ret = -1;
                break;
        }

        /* Respond appropriatly depending on command outcome */
        rsp_write(ret ? RSP_FAIL:RSP_DONE);

        /* FIXME : keep sas_dev running as long as there is no fatal crash */
        if(ret != 0) {
            ret = EXIT_FAILURE;
            goto _out;
        }
    }

    ret = EXIT_SUCCESS;

_out:
    if(global_mem != NULL) {
        free(global_mem);
    }
    if(arguments != NULL) {
        free(arguments);
    }

    close(kernel_fd);

    close(data_wr_fifo);
    close(data_rd_fifo);
    close(cmd_fifo);
    close(rsp_fifo);

    remove(DATA_RD_PIPE_PATHNAME);
    remove(DATA_WR_PIPE_PATHNAME);
    remove(CMD_PIPE_PATHNAME);
    remove(RSP_PIPE_PATHNAME);

    return ret;
}