#include "sas_dev.h"

#include <poll.h>

ssize_t
write_pipe(int pipe_fd, void *src, uint64_t len)
{
    void *src_p = src;
    size_t to_write = len;
    ssize_t nb_written;
    int ret;

    struct pollfd pfd = {
        .fd = pipe_fd,
        // Poll for data-out avail., err, hang up and invalid req
        .events = POLLHUP | POLLERR | POLLNVAL | POLLOUT,
    };

    nfds_t nfds = 1;
    int timeout = -1; // Indefinite timeout

    #ifdef DEBUG
    printf("writing %"PRIu64" bytes to pipe from address %p\n", len, src);
    #endif

    while(1) {
        if(to_write == 0) break;

        /* Wait for an event */
        ret = poll(&pfd, nfds, timeout);
        PERROR_AND_RETURN_ON((ret < 0), -1, "write_pipe poll");

        if(pfd.revents & POLLNVAL) {
            fprintf(stderr, "FD not open\n");
            return -1;
        }
        else if(pfd.revents & POLLHUP) {
            fprintf(stderr, "Connection hung up\n");
            return -1;
        }
        else if(pfd.revents & POLLERR) {
            fprintf(stderr, "Connection closed\n");
            return -1;
        }
        else if(pfd.revents & POLLOUT) {
            nb_written = write(pipe_fd, src_p, to_write);

            to_write -= nb_written;
            src_p = (char*)src_p + nb_written;

            if(nb_written < 0) return nb_written; // error code is returned
        }
    }

    return (ssize_t)len;
}

ssize_t 
read_pipe(int pipe_fd, void *dst, uint64_t len)
{
    void *dst_p = dst;
    size_t to_read = len;
    ssize_t nb_read;
    int ret;

    struct pollfd pfd = {
        .fd = pipe_fd,
        // Poll for data-in avail., err, hang up and invalid req
        .events = POLLHUP | POLLERR | POLLNVAL | POLLIN,
    };

    nfds_t nfds = 1;
    int timeout = -1; // Indefinite timeout

    #ifdef DEBUG
    printf("reading %"PRIu64" bytes from pipe to address %p\n", len, dst);
    #endif

    while(1) {
        if(to_read == 0) break;

        /* Wait for an event */
        ret = poll(&pfd, nfds, timeout);
        PERROR_AND_RETURN_ON((ret < 0), -1, "read_pipe poll");

        if(pfd.revents & POLLNVAL) {
            fprintf(stderr, "FD not open\n");
            return -1;
        }
        else if(pfd.revents & POLLHUP) {
            fprintf(stderr, "Connection hung up\n");
            return -1;
        }
        else if(pfd.revents & POLLERR) {
            fprintf(stderr, "Connection closed\n");
            return -1;
        }
        else if(pfd.revents & POLLIN) {
            nb_read = read(pipe_fd, dst_p, to_read);

            to_read -= nb_read;
            dst_p = (char*)dst_p + nb_read;

            if(nb_read < 0) return nb_read; // error code is returned
        }
    }

    return (ssize_t)len;
}

void
sas_to_pocl_context(struct pocl_context* pc, struct pocl_sas_context *psc)
{
    /* At compile time, if the target architecture is 32 bit, casting is forced
     * on 64 bit values. We ASSUME all 64 bit values don't overflow 32 bit width. */
    pc->work_dim = (uint32_t)psc->work_dim;
    pc->num_groups[0] = (size_t)psc->num_groups[0];
    pc->num_groups[1] = (size_t)psc->num_groups[1];
    pc->num_groups[2] = (size_t)psc->num_groups[2];
    pc->group_id[0] = (size_t)psc->group_id[0];
    pc->group_id[1] = (size_t)psc->group_id[1];
    pc->group_id[2] = (size_t)psc->group_id[2];
    pc->global_offset[0] = (size_t)psc->global_offset[0];
    pc->global_offset[1] = (size_t)psc->global_offset[1];
    pc->global_offset[2] = (size_t)psc->global_offset[2];
    pc->local_size[0] = (size_t)psc->local_size[0];
    pc->local_size[1] = (size_t)psc->local_size[1];
    pc->local_size[2] = (size_t)psc->local_size[2];
    pc->printf_buffer = (char*)psc->printf_buffer;
    pc->printf_buffer_position = (size_t *)psc->printf_buffer_position;
    pc->printf_buffer_capacity = (size_t)psc->printf_buffer_capacity;
}

