BUILD_DIR=.
SRC_DIR=.

CC=gcc

CFLAGS= -std=c11 -D_XOPEN_SOURCE=500 -Wall -Wextra -g
LDFLAGS= -ldl

BIN=sas_dev

SRC=$(wildcard $(SRC_DIR)/*.c)
OBJ=$(patsubst %.c,%.o,$(SRC))

.phony: clean

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) -o $(BUILD_DIR)/$@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf $(SRC_DIR)/*.o
