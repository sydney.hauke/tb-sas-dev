#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/mman.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <dlfcn.h>

#include "pocl_device.h"

#define DEBUG 1

/* Pipe locations */
// Read pipe from the client's side. We write it
#define DATA_RD_PIPE_PATHNAME       "/tmp/sas_data_rd"
// Write pipe from the client's side. We read it
#define DATA_WR_PIPE_PATHNAME       "/tmp/sas_data_wr"
// Command pipe. We read it
#define CMD_PIPE_PATHNAME           "/tmp/sas_cmd"
// Response pipe. We write it
#define RSP_PIPE_PATHNAME           "/tmp/sas_rsp"

/* Sockets */
// Data port
#define DATA_PORT                   3000
#define CMD_PORT                    3001

/* Command identifiers */
#define CMD_RD_BUF                  0x0
#define CMD_WR_BUF                  0x1
#define CMD_SET_DL_OBJ_PATH         0x2
#define CMD_SET_ARGS                0x3
#define CMD_SET_CONTEXT             0x4
#define CMD_START_COMP              0x5
#define CMD_GET_GLOBAL_BASE_ADDR    0x6
#define CMD_SEND_AND_SET_DL_OBJ     0x7

/* A command packet has three fields :
 * - command operand (1 byte)
 * - command argument 0 (8 bytes, may be empty but must be read)
 * - command argument 1 (8 bytes, may be empty but must be read)
 */
#define CMD_PACKET_SIZE             (17) // 17 bytes

/* Response identifiers */
#define RSP_DONE                    0x0
#define RSP_FAIL                    0x1

/* A response packet has only one field : a response code (32 bits) */
#define RSP_PACKET_SIZE             (1) // 1 byte

#define GLOBAL_MEM_SIZE             (1 << 25) // 32 MB

#define FILE_PATHNAME_LENGTH        1000
#define SYM_NAME_LENGTH             100

#define SERIALIEZED_KERNEL_PATH     "/tmp/sas_kernel.so"

#define PERROR_AND_RETURN_ON(cond, err_ret, err_str)            \
    do {                                                        \
        if(cond) {                                              \
            perror(err_str);                                    \
            return err_ret;                                     \
        }                                                       \
    } while(0)                                                 

/* Function declarations */
ssize_t
write_pipe(int pipe_fd, void *src, uint64_t len);

ssize_t 
read_pipe(int pipe_fd, void *dst, uint64_t len);

void
sas_to_pocl_context(struct pocl_context* pc, struct pocl_sas_context *psc);